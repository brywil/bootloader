/*********************************************************************
*
*                            Includes 
*
*********************************************************************/
#include <xc.h>
#include <p18cxxx.h>
#include "ecan.h"
#include "onyxx_can_frame.h"

/*********************************************************************
*
*                             Defines 
*
*********************************************************************/
// ECAN Operation mode
#define F_Lisn_mode     0      // 1 set ECAN module in listen only mode
#define F_Loop_mode     1      // 1 set ECAN module in loopback mode

unsigned long convertReg2ExtendedCANid(unsigned char tempRXBn_EIDH, unsigned char tempRXBn_EIDL, unsigned char tempRXBn_SIDH, unsigned char tempRXBn_SIDL);
unsigned long convertReg2StandardCANid(unsigned char tempRXBn_SIDH, unsigned char tempRXBn_SIDL);
void convertCANid2Reg(unsigned long tempPassedInID, unsigned char *passedInEIDH, unsigned char *passedInEIDL, unsigned char *passedInSIDH, unsigned char *passedInSIDL);
extern near uint8_t var_macaddr[MAX_MAC_SIZE+1];
extern near uint32_t var_macint;

/*********************************************************************
*
*                       Configure the CAN Module
*
*********************************************************************/
void InitECAN()
{
    CANCON = 0x80;  // Request configuration mode
    while (0x80 != (CANSTAT & 0xE0)); // wait for response
   
    // Mode 0

    ECANCON = 0;    // Mode 0
    CIOCONbits.ENDRHI = 1; // Without, we go into bus vapor lock!
    
    RXB0CONbits.RXM0 = 1;
    RXB0CONbits.RXM1 = 1;
    RXB1CONbits.RXM0 = 1;
    RXB1CONbits.RXM1 = 1;
    RXFCON0 = 0;
    RXFCON1 = 0;
    MSEL0 = 0xff;
    MSEL1 = 0xff;
    RXF0SIDLbits.EXIDEN = 0;
    RXF1SIDLbits.EXIDEN = 0;
                
    /* Initialize CAN Timings
    

	Baud rate: 1Mbps
	System frequency: 16000000 Hz
	Time quanta: 8
	Sample point: 1-1-4-2
	Sample point: 75.00%
    */
    
    BRGCON1 = 0x00; 
    BRGCON2 = 0x90;
    BRGCON3 = 0x02;   

    CANCON = 0x00; // Request normal mode
    while (0x00 != (CANSTAT & 0xE0)); // Wait for response
    
    LATB1 = 0;
}

/*********************************************************************
*
*                Check the buffers, if it have message
*
*********************************************************************/
bool ECAN_Receive(uCAN_MSG *tempCanMsg)
{
    bool retVal = false;
    uint8_t *p = &tempCanMsg->frame.data0;
    
    if (RXB0CONbits.RXFUL != 0)
    {
        if ((RXB0SIDL & 0x08) == 0x08) //If Extended Message
         {
             //message is extended
            tempCanMsg->frame.idType = (unsigned char) dEXTENDED_CAN_MSG_ID_2_0B;
            tempCanMsg->frame.id = convertReg2ExtendedCANid(RXB0EIDH, RXB0EIDL, RXB0SIDH, RXB0SIDL);

            tempCanMsg->frame.dlc = RXB0DLC;
            *p++ = RXB0D0;
            *p++ = RXB0D1;
            *p++ = RXB0D2;
            *p++ = RXB0D3;
            *p++ = RXB0D4;
            *p++ = RXB0D5;
            *p++ = RXB0D6;
            *p++ = RXB0D7;

            retVal = true;
        }
        
        RXB0CONbits.RXFUL = 0;
    } 
    
    // PIR5bits.RXB0IF = 0; //In Mode 1 this is RXBnIF, A CAN Receive Buffer has received a new message 
     
    return retVal;
}

/*********************************************************************
*
*                      Transmit Sample Message
*
*********************************************************************/
void ECAN_Transmit(uCAN_MSG *tempCanMsg) 
{
    unsigned char tempEIDH = 0;
    unsigned char tempEIDL = 0;
    unsigned char tempSIDH = 0;
    unsigned char tempSIDL = 0;
    unsigned char *p = &tempCanMsg->frame.data0;

    convertCANid2Reg(tempCanMsg->frame.id, &tempEIDH, &tempEIDL, &tempSIDH, &tempSIDL);

    TXB0EIDH = tempEIDH;
    TXB0EIDL = tempEIDL;
    TXB0SIDH = tempSIDH;
    TXB0SIDL = tempSIDL;
    
    TXB0DLC = tempCanMsg->frame.dlc;
    TXB0D0 = *p++;
    TXB0D1 = *p++;
    TXB0D2 = *p++;
    TXB0D3 = *p++;
    TXB0D4 = *p++;
    TXB0D5 = *p++;
    TXB0D6 = *p++;
    TXB0D7 = *p;

    TXB0CONbits.TXREQ = 1; //Set the buffer to transmit		
    
    return;
}

unsigned long convertReg2ExtendedCANid(unsigned char tempRXBn_EIDH, unsigned char tempRXBn_EIDL, unsigned char tempRXBn_SIDH, unsigned char tempRXBn_SIDL) 
{
    unsigned long ConvertedID = 0;
    //unsigned char CAN_standardLo_ID_lo2bits;
    //unsigned char CAN_standardLo_ID_hi3bits;

    //CAN_standardLo_ID_lo2bits = (tempRXBn_SIDL & 0x03);
    //CAN_standardLo_ID_hi3bits = (tempRXBn_SIDL >> 5);
    ConvertedID = (tempRXBn_SIDH << 3);
    ConvertedID = ConvertedID + (tempRXBn_SIDL >> 5); //CAN_standardLo_ID_hi3bits;
    ConvertedID = (ConvertedID << 2);
    ConvertedID = ConvertedID + (tempRXBn_SIDL & 0x03); // CAN_standardLo_ID_lo2bits;
    ConvertedID = (ConvertedID << 8);
    ConvertedID = ConvertedID + tempRXBn_EIDH;
    ConvertedID = (ConvertedID << 8);
    ConvertedID = ConvertedID + tempRXBn_EIDL;
    
    return (ConvertedID);
}

void convertCANid2Reg(unsigned long tempPassedInID, unsigned char *passedInEIDH, unsigned char *passedInEIDL, unsigned char *passedInSIDH, unsigned char *passedInSIDL) {
    unsigned char wipSIDL = 0;

    //EIDL
    *passedInEIDL = 0xFF & tempPassedInID; //CAN_extendedLo_ID_TX1 = &HFF And CAN_UserEnter_ID_TX1
    tempPassedInID = tempPassedInID >> 8; //CAN_UserEnter_ID_TX1 = CAN_UserEnter_ID_TX1 >> 8

    //EIDH
    *passedInEIDH = 0xFF & tempPassedInID; //CAN_extendedHi_ID_TX1 = &HFF And CAN_UserEnter_ID_TX1
    tempPassedInID = tempPassedInID >> 8; //CAN_UserEnter_ID_TX1 = CAN_UserEnter_ID_TX1 >> 8

    //SIDL
    //push back 5 and or it
    wipSIDL = 0x03 & tempPassedInID;
    tempPassedInID = tempPassedInID << 3; //CAN_UserEnter_ID_TX1 = CAN_UserEnter_ID_TX1 << 3
    wipSIDL = (0xE0 & tempPassedInID) + wipSIDL;
    wipSIDL = wipSIDL + 0x08; // TEMP_CAN_standardLo_ID_TX1 = TEMP_CAN_standardLo_ID_TX1 + &H8
    *passedInSIDL = 0xEB & wipSIDL; //CAN_standardLo_ID_TX1 = &HEB And TEMP_CAN_standardLo_ID_TX1

    //SIDH
    tempPassedInID = tempPassedInID >> 8;
    *passedInSIDH = 0xFF & tempPassedInID;
}
