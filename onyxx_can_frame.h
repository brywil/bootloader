#ifndef _ONYXX_CAN_FRAME_H
#define _ONYXX_CAN_FRAME_H
// v1.2
/*********************************************************************
*                             Defines
*********************************************************************/
#define TRUE    1
#define FALSE   0

#define CAN_MAX_DLEN 8

#define MAC_UNKNOWN -1
#define MAC_BAD_UID -2
/* First 3 bytes of LynxSpring UID MAC Address.  All MACS must be preceeded by this UID. */

#define LS_UID1		((unsigned char) 0x98)
#define LS_UID2		((unsigned char) 0xf0)
#define LS_UID3		((unsigned char) 0x58)

#define GET_BIT(p, n) ((p[n/8] >> (n%8)) & 0x01)
#define SET_BIT(p, n) p[n/8] |= (1 << (n%8))
#define CLR_BIT(p, n) p[n/8] &= (~(1 << (n%8)))

/* Onyxx Provides 10 x D0, 8 x A0 and 16 X UI interfaces.
 * Defines Onyxx Command IDs for points.  All points (from Canbus perspective)
 * are WRITE ONLY.
 */

#define D0_COUNT        10
#define D0_DATATYPE     uint8_t
#define D0_SIZE         sizeof(D0_DATATYPE)

#define A0_COUNT        8
#define A0_DATATYPE     uint16_t
#define A0_SIZE         sizeof(A0_DATATYPE)

#define UI_COUNT        16
#define UI_DATATYPE     uint16_t
#define UI_SIZE         sizeof(UI_DATATYPE)

#define MODE_COUNT      UI_COUNT
#define MODE_DATATYPE   uint8_t
#define MODE_SIZE       sizeof(MODE_DATATYPE)

#define MAX_POINTS      (A0_COUNT + D0_COUNT + UI_COUNT + MODE_COUNT)

/* Defines Onyxx Command IDs for special operations (S=1) */

typedef enum {
	CMD_ANNOUNCE_IND	= 0x01,
	CMD_RESET_IND		= 0x02,
	CMD_CODE_REQ		= 0x03,
	CMD_CODE_REP		= 0x04,
	CMD_HEXDATA_IND		= 0x05,
	CMD_HEXDATA_RESP	= 0x06,
	CMD_HEXLAST_IND		= 0x07,
	CMD_HEXLAST_RESP	= 0x08,
	CMD_PGM_MAC_IND		= 0x09,
	CMD_GET_MAC_REQ		= 0x0a,
	CMD_GET_MAC_REP 	= 0x0b,
	CMD_APP_ID_REQ		= 0x0c,
	CMD_APP_ID_REP   	= 0x0d,
	CMD_GET_CONFIG_REQ 	= 0x0e,
        CMD_GET_CONFIG_REP      = 0x0f,
	CMD_SET_CONFIG_REQ 	= 0x10,
        CMD_SET_CONFIG_REP      = 0x11
} onyxx_cmd_t;

#define MAX_MAC_SIZE            3
#define MAX_DEVICES             9
#define ONYXX_MODE_RESISTANCE_V 1
#define ONYXX_MODE_VOLTAGE_V	2
#define ONYXX_MODE_CURRENT_V	3

#define DEVICE_ID_UNUSED		((int) -1) // Indicates a device has not been allocated by the device manager.

/* Onyxx Provides 10 x D0, 8 x A0 and 16 X UI interfaces.
 * Defines Onyxx Command IDs for points.  All points (from Canbus perspective)
 * are WRITE ONLY.
 */

typedef enum {
    TYPE_LSM	= 1,
    TYPE_MLSM	= 2,
    TYPE_LED	= 3,
    TYPE_D0     = 4,
    TYPE_A0     = 5,
    TYPE_RESV   = 6,
    TYPE_UI_L   = 7,
    TYPE_UI_H   = 8,
    TYPE_MODE_L	= 9,
    TYPE_MODE_H = 10,
    TYPE_ID	= 11
} point_type_e;

// POPULATE bit settings

#define HAS_ONE         0
#define HAS_MANY        1

// CMD/DATA bit settings

#define TYPE_COMMAND    0
#define TYPE_DATA	1

/*
 * Controller Area Network Identifier structure
 *
 * The following macros allow us to manipulate the 29-bit ecan address
 * into specific fields.
 *
 * 32 Bit ID where lower 29 bits are relevant
 *
 * Bits 31-29       : Unused
 * Bits 28-26       : 3 bits for priority
 * Bit  25          : 1 bit for Populate (1=Has many, 0=Has one)
 * Bits 24          : 1 bit for Cmd/Data (1=command, 0=data)
 * Bits 23-0        : 24 bits for MAC address
 */

typedef union {
    struct {
        unsigned    mac2        : 8;
        unsigned    mac1        : 8;
        unsigned    mac0        : 8;
        unsigned    dtyp        : 1;
        unsigned    populate    : 1;
        unsigned    priority    : 3;
        unsigned    unused      : 3;
    } bits;

    uint32_t    val;
} lpp_id;

// Master identification

#define LSM_IDENTIFICATION      1
#define MLSM_IDENTIFICATION     2

#endif
