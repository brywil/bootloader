/************************************************************************
 * Program      : OnyxxFirmwareLoader
 * Author       : B. Wilcutt
 * Date         : 11.17.2016
 * Description  :
 * 
 * This code performs the codeload operation of the Onyxx platform for
 * CANBus drivers.  It is meant to operate on a PIC18F46K80 MCU.  Upon
 * boot, this software will wait up to WAIT_SECONDS seconds for a 
 * codeload event to occur, otherwise it begins executing the existing
 * firmware (if any).  See the PDD document for protocol description.
 * 
 * IMPORTANT NOTE ABOUT HEX FILE FORMATS
 * 
 * For files built in MPLAB IDE, assure the following has been met:
 * 
 * 1. Files are ASCII readable in IHEX format with CR/LF endings
 * 2. Uses the following configuration:
 *    Linker - Runtime Settings
 *      Clear BSS ON
 *      Initialize Data ON
 *      Format hex file for download ON
 *    Linker - Memory Model
 *      ROM Ranges: default, -0-7ff
 *      Code Offset: 0x800
 * 
 * For THIS bootloader, make sure you are using the following settings:
 * 
 * Linker - Runtime Settings
 *      Clear BBS ON
 *      Initialize Data ON
 * Linker - Memory Model
 *      ROM Ranges: leave this blank
 *      Code Offset: leave this blank
 * 
 * Without these settings it is unlikely the bootloader will be able to use the
 * HEX file since it is limited (due to space) in its ability to adapt to
 * other formats.
 * 
 * For CHIPID -- the var_devid below is either set to TYPE_MLSM for an MLSM or
 * to -1 for AUTO DISCOVERY.  Use -1 for all but the MLSM chip.
 ************************************************************************/

#define _XTAL_FREQ 15625000L

#pragma psect text=bootldr

#include "config.h"

#include <xc.h>
#include <string.h>
#include <p18cxxx.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include "ecan.h"
#include "onyxx_can_frame.h"

/* User ID words must be pre-initialized in a pragma, do not change these. */
/* Id words are nibbles, not bytes. */

#pragma config IDLOC0 = 0xf, IDLOC1 = 0xf;
#pragma config IDLOC2 = 0xf, IDLOC3 = 0xf;
#pragma config IDLOC4 = 0xf, IDLOC5 = 0xf;
#pragma config IDLOC6 = 0xf, IDLOC7 = 0xf;

/* The processor's interrupt table sits within the boot sector.  Therefore, we must route those interrupts
   from ourself to the application's interrupt table which is located at the beginning of the application.  
   This increases interrupt latency by 2 instructions.

	Table format:
		Reset Vector			App Address
		Hi Interrupt Vector		App Address + 0x8
		Lo Interrupt Vector		App Address + 0x18
*/

#asm

// Redirection of high priority interrupts to application
psect intcode 
	goto	PRGM_ADDR + 0x08;

// Redirection of low priority interrupts to application
psect intcodelo	
	goto	PRGM_ADDR + 0x18;

#endasm

#define WRITE 0
#define ERASE 1
#define USER_ID_SPACE    0x200000
#define CONFIG_REG_SPACE 0x300000

static void initDevice();
static void codeload();
static void writeFlash(uint32_t writeAddr, uint8_t *flashWrBufPtr, uint8_t bufSize);
static void pushFlash(uint32_t, bool);
static bool processLine();
static void programMAC(uint8_t *);
static void writeRegisters(uint8_t *flashWrBufPtr, uint8_t bufSize);
static void writeBuffer(uint32_t addr);

#define FLASH_BLOCKSIZE    64

uint32_t blockIndex;
uint8_t dataBlock[FLASH_BLOCKSIZE];

uint8_t var_macint[3];
uint8_t hexdata[32];
uint32_t blockStartAddr;

uCAN_MSG canmsg;
int8_t var_devid = TYPE_MLSM; // Default to MLSM, will change programmatically
uint32_t flashEnd = 0x10000; // 64K Default to MLSM, will change programmatically

void (*bootapp)() = (void *) PRGM_ADDR;

/******************************************************
 * Function     : main
 * Input        : None
 * Output       : None
 * Description  : Initializes the hardware platform
 *                and will wait up to 3 seconds for a 
 *                codeload event to be indicated via CANBus.  
 *                If no event, the PC is pointed to the internal 
 *                application to execute.  If a codeload event 
 *                is indicated via CANBus, software is downloaded.  
 *                Upon downloading, the system reboots back into 
 *                this software and the operation starts over
 *                again.
 ******************************************************/
void main()
{
    int16_t delay_count = 0;
    lpp_id lpp;
    uint8_t srcmac[3];
    
    blockStartAddr = 0;
    blockIndex = 0;
    
    // Upon boot up, the PIC chip executes the bootloader (this code).  We wait in
    // the bootloader for a predetermined number of milliseconds, looking for a 
    // codeload signal from the CAN Bus.  If one isn't received, we attempt to
    // execute whatever code is located in EEPROM.

    initDevice();

    while (1) 
    {
        /* Is there data on the CANBus to read? */
   
        __delay_ms(WAIT_MS_TIMER);  // WAIT_MS_TIMER * MAX_WAIT_LOOP indicates how long we wait for codeload
        
        if (ECAN_Receive((uCAN_MSG *) &canmsg))
        {
            /* Message received.  Is this a start code-load message? */
            
            lpp.val = canmsg.frame.id;
            
            if (lpp.bits.dtyp == TYPE_COMMAND)
            {
                switch(canmsg.frame.data1)
                {
                    case CMD_PGM_MAC_IND:
                        programMAC(&canmsg.frame.data2);
                        initDevice();
                        delay_count = -1;   // Stay in monitor mode
                        break;
                        
                    case CMD_GET_MAC_REQ:
                        lpp.bits.mac0 = canmsg.frame.data2;
                        lpp.bits.mac1 = canmsg.frame.data3;
                        lpp.bits.mac2 = canmsg.frame.data4;
                        lpp.bits.dtyp = TYPE_COMMAND;
                        lpp.bits.populate = HAS_ONE;
                        canmsg.frame.id = lpp.val;
                        canmsg.frame.data0 = var_devid;
                        canmsg.frame.data1 = CMD_GET_MAC_REP;
                        canmsg.frame.data2 = var_macint[0];
                        canmsg.frame.data3 = var_macint[1];
                        canmsg.frame.data4 = var_macint[2];
                        canmsg.frame.dlc = 5;
                        ECAN_Transmit(&canmsg);
                        
                        delay_count = -1; // Stay in monitor mode
                        break;
                        
                    case CMD_CODE_REQ:
                        if (lpp.bits.mac0 == var_macint[0] &&
                            lpp.bits.mac1 == var_macint[1] &&
                            lpp.bits.mac2 == var_macint[2] &&
                                canmsg.frame.data0 == var_devid)
                        {  
                            // Record requester's MAC which is a part of
                            // the Req message.
                            
                            srcmac[0] = canmsg.frame.data2;
                            srcmac[1] = canmsg.frame.data3;
                            srcmac[2] = canmsg.frame.data4;
                            
                            codeload(); // Load like the wind...
                           
                            lpp.bits.mac0 = srcmac[0];
                            lpp.bits.mac1 = srcmac[1];
                            lpp.bits.mac2 = srcmac[2];
                            lpp.bits.dtyp = TYPE_COMMAND;
                            lpp.bits.populate = HAS_ONE;
                            canmsg.frame.id = lpp.val;
                            canmsg.frame.data0 = var_devid;
                            canmsg.frame.data1 = CMD_CODE_REP;
                            canmsg.frame.data2 = var_macint[0];
                            canmsg.frame.data3 = var_macint[1];
                            canmsg.frame.data4 = var_macint[2];
                            canmsg.frame.dlc = 5;
                            ECAN_Transmit(&canmsg);

#asm
                            RESET;
#endasm
                        }
                    default:
                        break;
                }
            } 
        }   
        
        if (delay_count != -1)
        {
            if (delay_count++ > MAX_WAIT_LOOP)
            {    
                bootapp();
            }
        }
    }
}

/******************************************************
 * Function     : codeload
 * Input        : None
 * Output       : None
 * Description  : Performs a codeload by CANBus operation.
 *                Moves CAN packets consisting of 8 data bytes 
 *                to program memory.
 ******************************************************/
static void codeload()
{
    int i;
    uint8_t *p = NULL;
    uint8_t *h = hexdata;
    lpp_id lpp;
    bool finish = false;
    
     /* During codeload, if we wait longer than 100ms with no data, reboot. */
    
    p = &canmsg.frame.data2;

    // Continuing looping until we get the last record (CMD_CODESTOP).  Once we receive a
    // CODESTOP, we send one back to the far side to acknowledge we were successful, otherwise
    // we remain quiet indicating a failure.
    
    while (!finish)
    {
        if (ECAN_Receive((uCAN_MSG *) &canmsg))
        {
            // Be particular about what packet gets processed.
            
            if (canmsg.frame.dlc > 2 && (canmsg.frame.data1 == CMD_HEXDATA_IND || canmsg.frame.data1 == CMD_HEXLAST_IND))
            {
                lpp.val = canmsg.frame.id;
                
                // Verify this packet is destined for this device before processing further. 
                
                if (canmsg.frame.data0 == var_devid && 
                        var_macint[0] == lpp.bits.mac0 &&
                        var_macint[1] == lpp.bits.mac1 &&
                        var_macint[2] == lpp.bits.mac2)
                {
                    for (i = 0; i < (canmsg.frame.dlc - 2); *(h++) = p[i++]);
                    
                    if (canmsg.frame.data1 == CMD_HEXLAST_IND) {
                        // We're done with a single IHEX line, process it.

                        finish = processLine();
                        h = hexdata;
                    }
                }
            } 
        }
    }
}

/******************************************************
 * Function     : processLine
 * Input        : None
 * Output       : None
 * Description  : Processes a collected ihex line, converted
 *                to binary.  Parses ihex data types and
 *                verifies a record's checksum.
 ******************************************************/
static bool processLine()
{
    bool retVal = false; // Not end of file
    int i;
    uint8_t rsize = 0;
    uint8_t rtype = 0;
    uint8_t cksum = 0;
    uint32_t raddr = 0;
    static bool writeRegs = false;
    
    rsize = hexdata[0];
    ((uint8_t *) &raddr)[1] = hexdata[1];
    ((uint8_t *) &raddr)[0] = hexdata[2];
    rtype = hexdata[3];

    // Next, make sure checksum is correct

    cksum = 0;
    for (i = 0; i < (rsize + 4); i++) cksum += hexdata[i];
    cksum = (~cksum) + 1;
    
    if (cksum == hexdata[i])
    {
        switch(rtype)
        {
            case 0: // Standard data record
                if (writeRegs)
                {
                    writeRegisters(&hexdata[4], rsize);
                    writeRegs = false;
                } else {
                    writeFlash(raddr, &hexdata[4], rsize);
                }
                break;

            case 1: // End of file
                retVal = true; // we're done
                break;

            case 4: // Extended linear address record, special addresses
                // Flush buffer if it is still opened

                if (blockIndex != 0)
                {
                    writeBuffer(blockStartAddr);
                }

                writeRegs = true;

                break;

            default:
                retVal = true; // Error, unknown record type
                break;
        }
    } else {
        // Freeze -- bad checksum, don't continue.  Let client time out.
#asm        
        RESET;
#endasm        
    }
    
    return retVal;
}

/******************************************************
 * Function     : initDevice
 * Input        : None
 * Output       : None
 * Description  : Initializes the processor, interrupts
 *                the ECAN system.
 ******************************************************/
static void initDevice()
{
    // Initialize oscillator
     
    OSCCON = 0x70;  // SCS FOSC; HFIOFS not stable; IDLEN disabled; IRCF 8MHz_HF;
    OSCCON2 = 0x00; // SOSCGO disabled; MFIOSEL disabled; SOSCRUN disabled; MFIOFS not stable;
    OSCTUNE = 0x00; // INTSRC INTRC; PLLEN disabled; TUN 0; 
    REFOCON = 0x00; // ROSEL disabled; ROON disabled; ROSSLP disabled; RODIV Fosc;

    /* Our device ID is specified through a PORT.  We read ID_PIN0, ID_PIN1 and
     * ID_PIN2, mux them together and we'll have our ID.
     */

    LATB = 0x0;     // When running on a PICF Board with convertor
    TRISB1 = 0; // STBY

    ANCON1 = 0x00; // Set all pins to analog, without which we cannot read ChipID

    if (var_devid == -1)
    {
        var_devid = ((CHIP_ADDRESS_2 << 2) | (CHIP_ADDRESS_1 << 1) | (CHIP_ADDRESS_0)) + 2;
        flashEnd = 0x8000; // 32K
    }
    
    InitECAN();

    /* Obtain the MAC ID from flash */

    TBLPTRU = 0x20;
    TBLPTRH = 0x00;
    TBLPTRL = 0x00;

    asm("TBLRD*+");
    var_macint[0] = TABLAT;
    asm("TBLRD*+");
    var_macint[1] = TABLAT;
    asm("TBLRD*+");
    var_macint[2] = TABLAT;
    
    INTCONbits.GIE = 1; // Enable the Global Interrupts
    INTCONbits.PEIE = 1;// Enable the Peripheral Interrupts
}

/******************************************************
 * Function     : writeRegisters
 * Input        : uint8_t pointer to source buffer
 *                uint8_t buffer length
 * Output       : None
 * Description  : Writes data from pointer to the register block.
 *                Registers are written 1 byte at a time.
 ******************************************************/
static void writeRegisters(uint8_t *flashWrBufPtr, uint8_t bufSize)
{
    uint8_t i;
    
    // This function follows the PIC18F programming spec, pp 457.  Configuration bits
    // are written a single byte at a time.  Therefore, the byte must be erased,
    // moved to table then flashed.
    
    for (i = 0; i < bufSize; i++)
    {
        pushFlash(CONFIG_REG_SPACE + i, ERASE); // Erase, Set Address
        
        TBLPTRU = (uint8_t) 0x30;
        TBLPTRH = (uint8_t) 0x00;
        TBLPTRL = (uint8_t) i;  
        
        TABLAT = flashWrBufPtr[i];  // Write to Table
        asm("TBLWT*");

        // For registers, a write has to occur after each individual byte is
        // written.

        pushFlash(CONFIG_REG_SPACE + i, WRITE); // Move to Flash
    }
}

/******************************************************
 * Function     : writeBuffer
 * Input        : uint32_t address of destination flash mem
 * Output       : None
 * Description  : Writes 64-byte content of dataBlock to flash
 *                starting at given flash address.  Will first
 *                erase the block before writing.
 *                This function assumes the flash address
 *                begins on a Flash Row (64 byte boundary).
 ******************************************************/
static void writeBuffer(uint32_t addr)
{
    uint8_t i;
    
    pushFlash(addr, ERASE);

    TBLPTRU = (uint8_t) ((addr >> 16) & 0xff);
    TBLPTRH = (uint8_t) ((addr >> 8) & 0xff);
    TBLPTRL = (uint8_t) (addr & 0xff);    

    for (i = 0; i < FLASH_BLOCKSIZE; i++)
    {
        TABLAT = dataBlock[i];  // Load data byte
        asm("TBLWT*+");
        dataBlock[i] = 0xff;    // Clear out incase of overrun
    }

    asm("TBLRD*-");
    
    pushFlash(addr, WRITE);
}

/******************************************************
 * Function     : writeFlash
 * Input        : Destination flash address
 *                uint8_t ptr to source data to write
 *                uint8_t size of data to write
 * Output       : None
 * Description  : Writes flash data.  Will read existing 64
 *                block and update with new flash data if
 *                new flash data is < 64.  Otherwise, will
 *                not pre-read flash block.
 ******************************************************/
static void writeFlash(uint32_t writeAddr, uint8_t *flashWrBufPtr, uint8_t bufSize)
{
    uint8_t i = 0;

    // Is our write address beyond the current block we are collecting?  If so,
    // write the current block out and start our buffer over again using the
    // new write address.

    if ((writeAddr - blockStartAddr > (FLASH_BLOCKSIZE - 1)) && blockStartAddr != 0)
    {
        writeBuffer(blockStartAddr);
    }
        
    if (blockStartAddr == 0)
    {
        blockStartAddr = ((flashEnd-1) ^ (FLASH_BLOCKSIZE-1));
        blockStartAddr &= writeAddr;
    }
    
    for (i = 0; i < bufSize; i++)
    {
        dataBlock[blockIndex++] = flashWrBufPtr[i];
        
        if (blockIndex == FLASH_BLOCKSIZE)
        {
            // Block is full, write it.
            
            writeBuffer(blockStartAddr);
        }
    }
}

/******************************************************
 * Function     : pushFlash
 * Input        : int erase (1 or 0 for true or false)
 * Output       : None
 * Description  : Help function performs commonly executed code
 *                for writing to flash.
 ******************************************************/
static void pushFlash(uint32_t addr, bool action)
{
    // Write sequence follows steps found on pp 135 of PIC Programming Manual.
    // "7.5.1 Flash Program Memory Write Sequence"  Don't reset the table
    // pointers if we are writing to memory, this will upset the write
    // process.

    if (action)
    {
        TBLPTRU = (uint8_t) ((addr >> 16) & 0xff);
        TBLPTRH = (uint8_t) ((addr >> 8) & 0xff);
        TBLPTRL = (uint8_t) (addr & 0xff);    
    
        EECON1bits.FREE = 1;
    }
    
    EECON1bits.EEPGD = 1;
    EECON1bits.CFGS = 0;
    EECON1bits.WREN = 1;
    
    INTCONbits.GIE = 0; // Disable interrupts
    EECON2 = 0x55;
    EECON2 = 0xAA;
    EECON1bits.WR = 1;  // Write like the wind!
    INTCONbits.GIE = 1;   // Restore interrupt enable
    EECON1bits.WREN = 0;    // Disable writes to memory    
    
    blockStartAddr = 0; // Force new calculation
    blockIndex = 0;
}

/******************************************************
 * Function     : programMAC
 * Input        : gloabl canmsg pointer
 * Output       : None
 * Description  : Using D2, D3 and D4 of canmsg, will write
 *                MAC to 0x20 ID flash space of device.
 ******************************************************/
static void programMAC(uint8_t *flashWrBufPtr)
{
    uint8_t i;
    
    TBLPTRU = 0x20;
    TBLPTRH = 0;
    TBLPTRL = 0;       
    
    for (i = 0; i < FLASH_BLOCKSIZE; i++)
    {
        asm("TBLRD*+");
        dataBlock[i] = TABLAT;
    }
    
    dataBlock[0] = flashWrBufPtr[0];
    dataBlock[1] = flashWrBufPtr[1];
    dataBlock[2] = flashWrBufPtr[2];
    
    writeBuffer(USER_ID_SPACE);
}    
